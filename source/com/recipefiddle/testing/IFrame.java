package com.recipefiddle.testing;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.openqa.selenium.internal.Locatable;

/**
 * Used to test IFrame functionality -- Firefox does not work in a simple
 * use case, but Chrome is fine.
 */
public class IFrame {
  public static void main( String args[] ) throws Exception {
    System.setProperty( "webdriver.chrome.driver", "/opt/bin/chromedriver" );

    //WebDriver driver = new ChromeDriver();
    WebDriver driver = new FirefoxDriver();
    driver.manage().timeouts().implicitlyWait( 5, TimeUnit.SECONDS );
    driver.get( "http://localhost/iframe/" );

    driver.switchTo().frame( 0 );

    // Wait up to 2 seconds for elements to appear.
    WebDriverWait wait = new WebDriverWait( driver, 2 );

    // Wait for the iframe body to become visible.
    WebElement body = wait.until(
      ExpectedConditions.visibilityOfElementLocated( By.tagName( "body" ) ) );

    body.click();
    body.sendKeys( "Hello, world!" );
  }
}

