package com.recipefiddle.testing;

import java.awt.AWTException;
import java.awt.Robot;

import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.Arrays;


import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Window;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.interactions.Actions;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.openqa.selenium.internal.Locatable;


/**
 * Simplifies common tasks using Selenium 2.
 *
 * @see ScriptHarness
 */
public abstract class TestHarness {
  private final static int BROWSER_HEADER = 130;

  // Browser windows cannot be configured smaller than 640x480.
  private final static int MIN_BROWSER_WIDTH = 640;
  private final static int MIN_BROWSER_HEIGHT = 480;

  private final static int POSITION_X = 768;
  private final static int POSITION_Y = 288;

  private final static int OFFSET_X = 0;
  private final static int OFFSET_Y = 20;

  // TODO: Create enumerated type...
  protected final static int USE_ID        = 0;
  protected final static int USE_CSS       = 1;
  protected final static int USE_XPATH     = 2;
  protected final static int USE_TAG       = 3;
  protected final static int USE_LINK_TEXT = 4;

  // Name of the web browser's WebDriver to instantiate.
  private String browser = "chrome";

  // Used for interacting with the web page.
  private WebDriver driver;

  // Handles moving the mouse.
  private Robot robot;

  // Delay before action in milliseconds.
  private long actionDelay;

  // Defines the element search (DOM traversal match) mechanism.
  private int findBy = USE_ID;

  // Caches the page dimensions after the page loads. This is required
  // so that switching to frames, which have different page dimensions,
  // does not affect the on-screen mouse movement.
  private int pageWidth;
  private int pageHeight;

  // Default dimensions of 720p (1280x720 pixels).
  private int browserWidth = 1280;
  private int browserHeight = 720;

  // Indicates whether a click should occur prior to action execution.
  private boolean click = true;

  /**
   * Default (empty) constructor.
   */
  public TestHarness() throws Exception {
  }

  /**
   * Called after setup and before teardeown.
   */
  protected abstract void execute() throws Exception;

  /**
   * Calls setup, execute, and teardown. Subclasses can override as
   * necessary.
   */
  protected void run() throws Exception {
    setup();
    execute();
    teardown();
  }

  /**
   * Sets up the browser type and initial window. Note that the order is
   * important: the cookie for a given site cannot be set before
   * visiting the site's corresponding web page.
   */
  protected void setup() throws Exception {
    setupBrowser();
    setupWindow();
    setupDriver();
  }

  /**
   * Configures the driver to wait several seconds before declaring a
   * missing element.
   */
  protected void setupDriver() {
    getDriver().manage().timeouts().implicitlyWait(
      getDriverTimeout(), TimeUnit.SECONDS );
  }

  /**
   * Returns the length of time before a missing element causes a
   * timeout exception to be thrown. The return value is given in
   * seconds.
   *
   * @return A default value of 5.
   */
  protected int getDriverTimeout() {
    return 5;
  }

  protected void setupBrowser() {
    System.setProperty( "webdriver.chrome.driver", "/opt/bin/chromedriver" );
  }

  /**
   * Emulates a single key press with modifiers in the browser. This should
   * be used for sending non-text keystrokes, such as DIVIDE on the numeric
   * keypad, arrow keys, and such.
   * 
   * @param modifiers The meta keys to press down.
   * @param keystroke The single keystroke to send (with meta keys).
   * @see https://selenium.googlecode.com/git/docs/api/java/org/openqa/selenium/Keys.html
   */
  protected void sendKey( Keys[] modifiers, CharSequence keystroke )
    throws Exception {
    Actions actions = createActions();
    
    // Hold down any modifier keys (e.g., SHIFT, CONTROL).
    for( Keys key : modifiers ) {
      actions.keyDown( key );
    }

    // Send the keystroke (e.g., DEL, END, DIVIDE, etc.).
    actions.sendKeys( keystroke );
    
    // Release the modifiers keys.
    for( Keys key : modifiers ) {
      actions.keyUp( key );
    }

    // Execute the send key actions.    
    actions.build();
    actions.perform();
  }
  
  /**
   * Emulates a single key press. The element should have focus prior to
   * calling this method.
   *
   * @param lookup The element to receive a keystroke.
   * @param keystroke The keystroke to send to the element.
   */
  protected void keystroke( String lookup, String keystroke )
    throws Exception {
    // Find the element that will recieve the given keystroke.
    WebElement element = find( lookup );

    // Send the given keystroke to the element.
    element.sendKeys( Keys.valueOf( keystroke ) ); 
  }
  
  /**
   * Sets the initial view for the browser, which includes the browser's
   * dimensions as well as its position on the screen. This will also
   * remove the "add-on" bar.
   */
  protected void setupWindow()
    throws Exception {
    WebDriver driver = getDriver();
    Window window = driver.manage().window();

    window.setSize( new Dimension( getBrowserWidth(), getBrowserHeight() ) );
    window.setPosition( new Point( POSITION_X, POSITION_Y ) );

    // Remove the "add-on" bar.
    driver.switchTo().defaultContent();
    sendKey( new Keys[]{ Keys.CONTROL }, Keys.DIVIDE );

    // Return to the main window.
    driver.switchTo().defaultContent();
  }

  /**
   * Closes the browser.
   */
  protected void teardown() throws InterruptedException {
    pause( 5000 );
    getDriver().quit();
  }

  /**
   * Instantiates a By instance according to the value returned from
   * getFindBy(). This can be used with getDriver().findElement() to
   * retrieve the actual element that corresponds to the lookup value.
   *
   * @param lookup The value that is a CSS selector, XPath, ID, or tag
   * name.
   * @return The By instance that can be used to find the WebElement
   * instance for the lookup parameter.
   */
  protected By findBy( String lookup ) throws Exception {
    By result;

    switch( getFindBy() ) {
      case USE_CSS:
        result = By.cssSelector( lookup );
        break;
      case USE_XPATH:
        result = By.xpath( lookup );
        break;
      case USE_TAG:
        result = By.tagName( lookup );
        break;
      case USE_LINK_TEXT:
        result = By.linkText( lookup );
        break;
      case USE_ID:
      default:
        result = By.id( lookup );
        break;
    }

    return result;
  }

  /**
   * Finds an element based on its lookup value.
   *
   * @return The WebElement corresponding to the lookup parameter.
   *
   * @see findBy
   */
  protected WebElement find( String lookup ) throws Exception {
    return getDriver().findElement( findBy( lookup ) );
  }

  /**
   * Moves the mouse to an element on the web page.
   *
   * @return Returns the WebElement that was moved to.
   *
   * @see https://github.com/Ardesco/Powder-Monkey/blob/master/src/main/java/com/lazerycode/selenium/tools/RobotPowered.java
   * @see http://ardesco.lazerycode.com/index.php/2012/12/hacking-mouse-move-events-into-safari-driver-the-nasty-way/
   */
  private WebElement moveTo( WebElement element ) throws Exception {
    WebDriver driver = getDriver();
    WebDriver.Window window = driver.manage().window();
    Dimension size = window.getSize();

    int browserWidth = size.getWidth();
    int browserHeight = size.getHeight();

    int offsetX = browserWidth - getPageWidth();
    int offsetY = browserHeight - getPageHeight();

    Point point = ((Locatable)element).getCoordinates().inViewPort();
    size = element.getSize();

    int elementX = point.getX() + Math.round(size.getWidth() / 2);
    int elementY = point.getY() + Math.round(size.getHeight() / 2);

    point = window.getPosition();

    int x = point.getX() + offsetX + elementX;
    int y = point.getY() + offsetY + elementY;

    mouseMove( x, y );

    Actions actions = createActions();
    actions.moveToElement( element ).build().perform();

    // Stop after moving prior to firing an event (e.g., click).
    postMoveToPause();

    return element;
  }

  /**
   * Helper method.
   */
  private WebElement moveTo( String lookup ) throws Exception {
    return moveTo( find( lookup ) );
  }

  /**
   * Instructs the browser to open a given web page. This should happen
   * before setting a cookie.
   *
   * @param url The address of the web page to visit.
   */
  protected void visit( String url ) throws Exception {
    getDriver().get( url );
  }

  /**
   * Updates the value of a cookie. This can only happen after visiting 
   * the web page that uses said cookie.
   *
   * @param name The cookie's identifier.
   * @param value The information associated with the cookie.
   */
  protected void cookie( String name, String value ) throws Exception {
    Cookie cookie = new Cookie( name, value );
    getDriver().manage().addCookie( cookie );
  }

  /**
   * Moves the mouse to the given element and then clicks it.
   *
   * @param element The element to move to and click upon.
   */
  protected WebElement click( WebElement element )
    throws Exception {
    element = moveTo( element );

    if( getClick() ) {
      element.click();
    }

    return element;
  }

  /**
   * Moves the mouse to a web page element and then clicks it. Client
   * classes can change the contextual meaning of the "lookup" parameter
   * by calling setFindBy with USE_ID, USE_CSS, etc. This gives fine
   * control over how elements are referenced. Elements should primarily
   * be referenced using the ID with only notable exceptions for using
   * other expression types.
   *
   * @param lookup String used to find elements by CSS selector, ID
   * attribute, etc.
   */
  protected WebElement click( String lookup )
    throws Exception {
    return click( find( lookup ) );
  }

  /**
   * Appends the given content to the element specified by the given id.
   *
   * @param lookup The form element to receive text.
   * @param content The text to put into the element.
   */
  protected WebElement text( String lookup, String content )
    throws Exception {
    WebElement element = click( lookup );
    element.sendKeys( content );
    return element;
  }

  /**
   * Switches to a contentEditable context.
   *
   * @param content The text to write in the contentEditable area.
   */
  protected WebElement contentEditable( String lookup, String content )
    throws Exception {
    WebElement element = getDriver().switchTo().activeElement();
    moveTo( lookup );
    element.sendKeys( content );
    return element;
  }

  /**
   * Clicks a given option from a list of options (does not perform
   * multi-select).
   *
   * @param lookup The form element to receive text.
   * @param text The text in the selection list to select.
   */
  protected WebElement select( String lookup, String text )
    throws Exception {
    assert lookup != null;
    assert text != null;

    // Cannot click a multi-select element.
    WebElement element = moveTo( lookup );

    Select select = new Select( element );
    select.selectByVisibleText( text );

    return element;
  }

  /**
   * Moves the cursor to the given element and then executes CTRL+a to
   * highlight all the text.
   */
  protected void highlight( String lookup ) throws Exception {
  }

  /**
   * This will highlight text (typically in a text area input field)
   * starting from the first matching regular expression value that
   * corresponds to the "from" parameter. The highlighting will include
   * and stop at the "to" parameter.
   * 
   * @param lookup The element that contains text to highlight.
   * @param from The starting text (a regular expression, inclusive).
   * 
   * @return The WebElement that has selected text.
   * @see http://stackoverflow.com/a/9984756/59087
   * @see http://selenium.googlecode.com/git/docs/api/java/org/openqa/selenium/interactions/Actions.html
   * @see http://www.regexplanet.com/advanced/java/
   */
  protected WebElement highlight( String lookup, String from )
    throws Exception {
    
    // 1. Move the cursor to the element with text to be highlighted.
    moveTo( lookup );
    
    // 2. Find the starting text index in the element's text, which
    // becomes the cursor's position for highlighting the text via
    // the keyboard.
    
    // 3. Move the cursor to that starting offset in preparation to
    // highlight from the cursor to the end of the text.
    
    // 4. Send CONTROL+SHIFT and END key to higlight the text by
    // emulating keystrokes.
    sendKey( new Keys[]{ Keys.CONTROL, Keys.SHIFT }, "Keys.END" );
    
    return null;
  }

  /**
   * Performs the drag portion of a drag-and-drop operation starting from
   * the element referenced by the "lookup" parameter.
   *
   * @param lookup The element to start dragging at (receives the drag event).
   * @return The element that corresponds to the lookup value.
   */
  protected void drag( String lookup ) 
    throws Exception {
    createActions().clickAndHold( moveTo( lookup ) ).perform();
  }

  /**
   * Performs the drop portion of a drag-and-drop operation, ending at
   * the element referenced by the "lookup" parameter.
   *
   * @param lookup The element to stop dragging at (recieves the drop event).
   * @return The element that corresponds to the lookup value.
   */
  protected void drop( String lookup )
    throws Exception {
    createActions().release( moveTo( lookup ) ).perform();
  }

  /**
   * Emulates a CTRL+c event to instruct the browser to copy the highlighted
   * text into the paste buffer.
   */
  protected void copy() throws Exception {
    sendKey( new Keys[]{ Keys.CONTROL }, "c" );
  }

  /**
   * Emulates a CTRL+v event to instruct the browser to paste the text from
   * the paste buffer.
   */
  protected void paste() throws Exception {
    sendKey( new Keys[]{ Keys.CONTROL }, "v" );
  }

  /**
   * Emulates a CTRL+x event to instruct the browser to cut (copy then delete)
   * the text into the paste buffer.
   */
  protected void cut() throws Exception {
    sendKey( new Keys[]{ Keys.CONTROL }, "x" );
  }

  /**
   * Causes the browser to refresh.
   */
  protected void refresh() {
    getDriver().navigate().refresh();
  }

  /**
   * Clears out the contents of a text field. This does not appear to work.
   *
   * @param id The form field identifier (input/textarea) to clear.
   */
  protected WebElement clear( String lookup )
    throws Exception {
    WebElement element = click( lookup );
    element.clear();
    return element;
  }

  /**
   * Switches context to the frame.
   *
   * @param index 0-based index of iframe elements on the web page.
   * @return A reference to the iframe's body element.
   */
  protected WebElement switchToFrame( int index )
    throws Exception {
    WebDriver driver = getDriver();
    driver.switchTo().frame( index );
    return driver.switchTo().activeElement();
  }

  /**
   * Helper method.
   */
  protected void switchToFrame( String index )
    throws Exception {
    // Crash if the index is not set properly.
    switchToFrame( Integer.parseInt( index ) );
  }

  /**
   * Forces the context back to the main page. This can be used to
   * escape from a frame.
   */
  protected void switchToDefault() {
    getDriver().switchTo().defaultContent();
  }

  /**
   * Asks Thread to put the execution to sleep for a given duration.
   *
   * @param duration Duration to wait in milliseconds.
   */
  protected void pause( long duration ) throws InterruptedException {
    Thread.sleep( duration );
  }

  /**
   * Helper method.
   */
  protected void pause( String duration ) throws InterruptedException {
    pause( Long.parseLong( duration ) );
  }

  /**
   * Pauses after moveTo is called.
   */
  protected void postMoveToPause() throws InterruptedException {
    pause( getActionDelay() );
  }

  /**
   * Moves the mouse to the given coordinates.
   */
  private void mouseMove( int x, int y ) throws AWTException {
    getRobot().mouseMove( x + OFFSET_X, y + OFFSET_Y );
    getRobot().waitForIdle();
  }

  /**
   * Returns the object used for moving the mouse cursor.
   */
  private Robot getRobot() throws AWTException {
    Robot robot = this.robot;

    if( robot == null ) {
      robot = createRobot();
      setRobot( robot );
    }

    return robot;
  }

  /**
   * Sets the object to use for moving the mouse cursor.
   */
  private void setRobot( Robot robot ) {
    this.robot = robot;
  }

  /**
   * Creates the object used for moving the mouse cursor.
   */
  protected Robot createRobot() throws AWTException { 
    return new EasingRobot();
  }

  /**
   * Changes the driver to use.
   */
  private void setDriver( WebDriver driver ) {
    this.driver = driver;
  }

  /**
   * Returns the web browser to use when performing the test. Subclasses
   * can override createDriver() to return a different default.
   *
   * @return FirefoxDriver.
   */
  private WebDriver getDriver() {
    WebDriver driver = this.driver;

    if( driver == null ) {
      driver = createDriver();
      setDriver( driver );
    }

    return this.driver;
  }

  /**
   * Returns the type of web browser to use when performing the test.
   *
   * @return ChromeDriver is the default browser for testing.
   */
  protected WebDriver createDriver() {
    WebDriver result;

    switch( getBrowser() ) {
      case "firefox":
        result = new FirefoxDriver();
        break;
      case "chrome":
      default:
        result = new ChromeDriver();
    }

    return result;
  }

  /**
   * Changes how long to wait before an action is executed.
   *
   * @param actionDelay Duration to wait in milliseconds.
   */
  protected void setActionDelay( long actionDelay ) {
    this.actionDelay = actionDelay;
  }

  /**
   * Helper method.
   *
   * @param actionDelay Duration to wait in milliseconds.
   */
  protected void setActionDelay( String actionDelay ) {
    // Only set the delay if there is valid text (i.e., a number) to
    // parse.
    if( valid( actionDelay ) ) {
      setActionDelay( Long.parseLong( actionDelay ) );
    }
  }

  /**
   * Changes how element searches are performed.
   *
   * TODO: Use an enumerated type.
   */
  protected void setFindBy( int findBy ) {
    this.findBy = findBy;
  }

  /**
   * Returns search type to perform when finding the actionable element.
   */
  private int getFindBy() {
    return this.findBy;
  }

  /**
   * Sets the answer to the question: to click or not to click prior to
   * executing an action.
   *
   * @param click The answer to the question (true means to click, which
   * should be the default value).
   */
  protected void setClick( boolean click ) {
    this.click = click;
  }

  /**
   * Sets the clicking to true for any value of click other than "false".
   *
   * @param click A case-insensitive boolean string (only a "false" value
   * will disable clicking); can be empty or null.
   */
  protected void setClick( String click ) {
    if( valid( click ) ) {
      setClick( Boolean.parseBoolean( click ) );
    }
    else {
      // If the click mechanism was not explicitly disabled, then
      // ensure it remains enabled.
      setClick( true );
    }
  }

  /**
   * To click or not to click, that is the question, and this method is
   * the answer. This is useful when the element has already been clicked
   * (such as clicking a dynamic edit-in-place field). Additional clicks
   * to an edit-in-place field can cause selected text to deselect, which
   * would result in the automated script to not replace text but append
   * instead.
   *
   * @return true Click on a text element before sending the text.
   */
  private boolean getClick() {
    return this.click;
  }

  /**
   * Returns the amount of time to wait after "moveTo" is called.
   */
  private long getActionDelay() {
    return this.actionDelay;
  }

  /**
   * Returns "0" if the JavaScript could not run; otherwise, it will return
   * the string representation of the object created from running the
   * JavaScript.
   */
  private String runJS( String js ) {
    Object result = ((JavascriptExecutor)getDriver()).executeScript( js );
    return result == null ? "0" : result.toString();
  }

  private void setPageWidth( String s ) {
    this.pageWidth = Integer.parseInt( s );
  }

  /**
   * Returns the width of the browser page.
   */
  private int getPageWidth() {
    int pw = this.pageWidth;

    if( pw == 0 ) {
      String s = runJS( "return document.documentElement.clientWidth" );
      setPageWidth( s );
    }

    return this.pageWidth;
  }

  private void setPageHeight( String s ) {
    this.pageHeight = Integer.parseInt( s );
  }

  /**
   * Returns the height of the browser page.
   */
  private int getPageHeight() {
    int ph = this.pageHeight;

    if( ph == 0 ) {
      String s = runJS( "return document.documentElement.clientHeight" );
      setPageHeight( s );
    }

    return this.pageHeight;
  }

  /**
   * Returns true for non-empty and non-null strings.
   */
  private boolean valid( String s ) {
    return s != null && s.length() > 0;
  }

  /**
   * Valid values include: firefox, chrome, ie, safari.
   */
  protected void setBrowser( String browser ) {
    assert browser != null;
    this.browser = browser;
  }

  /**
   * Returns the name of the browser WebDriver to instantiate.
   */
  private String getBrowser() {
    return this.browser;
  }

  /**
   * Returns the width dimension for the browser (including decorations).
   *
   * @return Typically 1024 or 1280.
   */
  private int getBrowserWidth() {
    return this.browserWidth;
  }

  /**
   * Returns the height dimension for the browser (including decorations).
   *
   * @return Typically 768 or 720.
   */
  private int getBrowserHeight() {
    return this.browserHeight;
  }

  /**
   * Changes the browser's width dimension.
   */
  public void setBrowserWidth( int browserWidth ) {
    if( browserWidth >= MIN_BROWSER_WIDTH ) {
      this.browserWidth = browserWidth;
    }
  }

  /**
   * Changes the browser's height dimension.
   */
  public void setBrowserHeight( int browserHeight ) {
    if( browserHeight >= MIN_BROWSER_HEIGHT ) {
      this.browserHeight = browserHeight;
    }
  }

  /**
   * Sets the browser window dimensions, given a string containing two
   * numbers. The string is defined by #.# where # is any number and
   * the period is any character.
   */
  public void setBrowserDimensions( String dimensions ) {
    String s[] = dimensions.split( "[^\\d]" );

    setBrowserWidth( Integer.parseInt( s[0] ) );
    setBrowserHeight( Integer.parseInt( s[1] ) );
  }

  /**
   * Returns an empty set of actions created against the WebDriver.
   *
   * @return A valid Actions instance.
   */
  private Actions createActions() {
    return new Actions( getDriver() );
  }
}

