package com.recipefiddle.testing;

import java.io.File;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.openqa.selenium.Keys;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;

/**
 * Runs a test script.
 */
public class ScriptHarness extends TestHarness {
  private Document document;
  private String browser = "chrome";

  public ScriptHarness() throws Exception {
  }

  /**
   * Loads an XML file and executes the contents of the script.
   *
   * @param script Name of the script to run.
   */
  protected void run( String script ) throws Exception {
    File file = new File( script );
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    setDocument( builder.parse( file ) );

    // Run the testing harness.
    run();
  }

  /**
   * Returns a list of nodes embedded within the root node.
   */
  protected NodeList getScriptNodes() {
    return getDocument().getDocumentElement().getChildNodes();
  }

  /**
   * Runs the script, calling the test harness to invoke each action read
   * from the script file.
   */
  protected void execute() throws Exception {
    execute( getScriptNodes() );
  }

  /**
   * Called to run a list of commands specified in the script. This is
   * called recursively for frame commands (that reference an iframe).
   *
   * @param nodes The list of nodes to run through Selenium.
   */
  protected void execute( NodeList nodes ) throws Exception {
    int length = nodes.getLength();

    for( int i = 0; i < length; i++ ) {
      Node node = nodes.item( i );
      Map<String, String> map = toMap( node );

      String id = map.get( "id" );
      String css = map.get( "css" );
      String xpath = map.get( "xpath" );
      String link = map.get( "link" );

      // Assume searching for the id.
      String lookup = id;
      setFindBy( USE_ID );

      if( css != null ) {
        lookup = css;
        setFindBy( USE_CSS );
      }
      else if( xpath != null ) {
        lookup = xpath;
        setFindBy( USE_XPATH );
      }
      else if( link != null ) {
        lookup = link;
        setFindBy( USE_LINK_TEXT );
      }

      setClick( map.get( "click" ) );
      setActionDelay( map.get( "delay" ) );

      switch( node.getNodeName() ) {
        case "visit": visit( node ); break;
        case "cookie": cookie( node, map ); break;
        case "click": click( lookup ); break;
        case "text": text( lookup, node, map ); break;
        case "option": option( lookup, map ); break;
        case "frame": frame( node, map ); break;
        case "pause": pause( map ); break;
        case "refresh": refresh(); break;
        case "key": keystroke( lookup, map ); break;
        case "drag": drag( lookup ); break;
        case "drop": drop( lookup ); break;
        case "highlight": highlight( lookup ); break;
        case "copy": copy(); break;
        case "cut": cut(); break;
        case "paste": paste(); break;
      }

      // Enable clicking again (does nothing if already set to true).
      setClick( true );
    }
  }

  /**
   * Directs the browser to a new URL.
   */
  protected void visit( Node node )
    throws Exception {
    visit( node.getTextContent() );
  }

  /**
   * Sets a cookie in the browser.
   */
  protected void cookie( Node node, Map<String, String> map )
    throws Exception {
    cookie( map.get( "name" ), node.getTextContent() );
  }

  /**
   * Sends text to the browser. This can change the behaviour of clicking
   */
  private void text( String lookup, Node node, Map<String, String> map )
    throws Exception {
    String text = node.getTextContent();

    if( Boolean.parseBoolean( map.get( "edit" ) ) ) {
      contentEditable( lookup, text );
    }
    else {
      text( lookup, text );
    }
  }

  /**
   * This helper method hides the "text" attribute lookup for a drop-down
   * list.
   *
   * @param map The map containing a value attribute.
   */
  private void option( String lookup, Map<String, String> map )
    throws Exception {
    select( lookup, map.get( "text" ) );
  }

  /**
   * Switches window context to an iframe, executes the scripts subscripts
   * within the iframe, then returns context to the default window.
   *
   * @param node The frame document node (should contain child nodes).
   * @param map The map of attributes.
   */
  private void frame( Node node, Map<String, String> map )
    throws Exception {
    switchToFrame( map.get( "index" ) );
    execute( node.getChildNodes() );
    switchToDefault();
  }

  /**
   * Momentarily pauses script execution using the delay attribute. This
   * will not affect the action delay.
   *
   * @param node The frame document node (should contain child nodes).
   * @param map The map of attributes.
   */
  private void pause( Map<String, String> map )
    throws Exception {
    pause( map.get( "delay" ) );
  }
  
  /**
   * Sends single keystroke.
   *
   * @param map The map containing a value attribute for desired key.
   */
  private void keystroke( String lookup, Map<String, String> map )
    throws Exception {
    keystroke( lookup, map.get( "code" ) );
  }
  
 /**
   * Initializes the attributes container.
   *
   * @return A map of the node attributes, or an emty map (never null).
   */
  private Map<String, String> toMap( Node root ) {
    Map<String, String> map = createAttributeMap();
    NamedNodeMap attributes = root.getAttributes();

    if( attributes != null ) {
      String nodeName = root.getNodeName();
      int length = attributes.getLength();

      for( int i = 0; i < length; i++ ) {
        Node node = attributes.item( i );
        String name = node.getNodeName(),
               value = node.getTextContent();
        map.put( name, value );

        log( "[%-7s] %5s = %s\n", nodeName, name, value );
      }
    }

    return map;
  }

  /**
   * Creates an attributes container.
   */
  private Map<String, String> createAttributeMap() {
    return new HashMap<String, String>();
  }

  /**
   * Sets the document containing the script to execute.
   *
   * @param document The script document.
   */
  private void setDocument( Document document ) {
    this.document = document;
  }

  /**
   * Returns the document that contains the script to execute.
   */
  private Document getDocument() {
    return this.document;
  }

  /**
   * Debugging.
   */
  private void log( String format, Object... args ) {
    System.err.printf( format, args );
  }

  /**
   * Runs a test script.

   * @param args[0] The name of the test script to run.
   */
  public static void main( String[] args ) throws Exception {
    ScriptHarness harness = new ScriptHarness();

    // Run the script using a different browser, if required.
    if( args.length > 1 ) {
      harness.setBrowser( args[1] );
    }

    if( args.length > 2 ) {
      harness.setBrowserDimensions( args[2] );
    }

    harness.run( args[0] );
  }
}
