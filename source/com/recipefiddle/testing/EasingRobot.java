package com.recipefiddle.testing;

import java.awt.AWTException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.Robot;

/**
 * Drop-in replacement for the Robot class that animates the mouse moving
 * from its current location to the location requested by mouseMove( x, y ).
 */
public class EasingRobot extends Robot {
  public EasingRobot() throws AWTException {
  }

  /**
   * Obtains the current mouse coordinates and returns them as an array of
   * two elements where [0] = x and [1] = y.
   *
   * @see http://stackoverflow.com/a/4484149/59087
   */
  private int[] getCurrentLocation() {
    PointerInfo pointer = MouseInfo.getPointerInfo();
    Point p = pointer.getLocation();

    // http://stackoverflow.com/a/1154027/59087
    return new int[]{ (int)p.getX(), (int)p.getY() };
  }

  /**
   * Returns the number of steps to go from point A to B.
   */
  protected int getSteps() {
    return 100;
  }

  /**
   * Returns the number of milliseconds to delay between each step of
   * the simulated mouse movement animation.
   */
  protected int getSpeed() {
    return 10;
  }

  /**
   * Performs a sinusoidal easing operation.
   *
   * @param t - Current step in the animation.
   * @param b - Starting value (e.g., starting X or Y value).
   * @param c - Difference between starting value and ending value.
   * @param d - Total steps in the animation (must be greater than 0).
   *
   * @see http://upshots.org/actionscript/jsas-understanding-easing
   * @see https://github.com/jesusgollonet/processing-penner-easing/tree/master/src
   * @see http://www.gizma.com/easing/
   */
  public double easing( double t, double b, double c, double d ) {
    assert d > 0;
    return -c/2 * ((float)Math.cos(Math.PI*t/d) - 1) + b;
  }

  /**
   * Moves the mouse from its current location to (x2,y2).
   *
   * @see http://stackoverflow.com/a/6147716/59087
   */
  public void mouseMove( int x2, int y2 ) {
    int start[] = getCurrentLocation();
    int x1 = start[0];
    int y1 = start[1];
    int steps = getSteps();
    int speed = getSpeed();

    for( int i = 0; i < steps; i++ ) {
      double dx = easing( i, x1, x2 - x1, steps );
      double dy = easing( i, y1, y2 - y1, steps );

      super.mouseMove( (int)dx, (int)dy );
      delay( speed );
    }
  }
}

